import {IComponentOptions} from 'angular';
import {AppController} from './app.controller';

export const appComponent: IComponentOptions = {
    templateUrl: require('./app.html'),
    controller: AppController,
};
