import {module} from 'angular';
import {appComponent} from './app.component';
import {componentsModule} from './components/components.module';
import {commonModule} from './common/common.module';

export const appName = module('app', [
    componentsModule,
    commonModule,
])
    .component('app', appComponent)
    .name;

