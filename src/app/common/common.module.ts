import {module} from 'angular';

export const commonModule: string = module('app__common',[])
    .name;