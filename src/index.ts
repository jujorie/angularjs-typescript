import '@uirouter/angularjs';
import 'ui-bootstrap4';
import {element, bootstrap} from 'angular';
import {appName} from './app/app.module'

element(() => {
    console.log('DOM Loaded');
    bootstrap(document, [
        'ui.router',
        'ui.bootstrap',
        appName
    ])
})